import Vue from 'vue'
import AppHeaderComponent from '@/components/AppHeaderComponent.vue'
import EmptyFolderMessageComponent from '@/components/EmptyFolderMessageComponent.vue'

Vue.component('AppHeaderComponent', AppHeaderComponent)
Vue.component('EmptyFolderMessageComponent', EmptyFolderMessageComponent)
